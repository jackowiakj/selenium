import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from homework_random import RandomUtil

@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('http://demo.testarena.pl/zaloguj')
    browser.find_element(By.CSS_SELECTOR, '#email').send_keys('administrator@testarena.pl')
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys('sumXQQ72$L')
    browser.find_element(By.CSS_SELECTOR, '#login').click()
    yield browser
    browser.quit()

def test_homework(browser):
    admin_button = browser.find_element(By.CSS_SELECTOR, '[title=Administracja]')
    admin_button.click()
    add_project_button = browser.find_element(By.CSS_SELECTOR, '.button_link')
    assert add_project_button.is_displayed()
    add_project_button.click()
    prefix = RandomUtil.get_random_string(4)
    name = f'4testers {prefix}'
    browser.find_element(By.CSS_SELECTOR, '#name').send_keys(name)
    browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(prefix)
    browser.find_element(By.CSS_SELECTOR, '#save').click()
    browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()
    browser.find_element(By.CSS_SELECTOR, 'search').send_keys(name)
    browser.find_element(By.CSS_SELECTOR, 'j_searchButton').click()
    assert browser.find_element(By.CSS_SELECTOR, '.t_number').text == prefix


